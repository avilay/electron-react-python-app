import React from 'react';
import ReactDOM from 'react-dom';
import App from './app/App';

window.onload = function(){
  ReactDOM.render(<App />, document.getElementById('app'));
}
